﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ResponsiveUIExample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Worker _worker = new Worker();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnSeq_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                txtSeq.Text += "Numer " + i + ": ";
                txtSeq.Text += _worker.GenerateNumber();
                txtSeq.Text += "\n";
            }
        }

        private void btnBad_Click(object sender, RoutedEventArgs e)
        {
            Parallel.For(0, 10, (i) =>
                {
                    txtSeq.Text += "Numer " + i + ": ";
                    txtSeq.Text += _worker.GenerateNumber();
                    txtSeq.Text += "\n";
                });
        }

        private void btnCon_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Implement a fix using the Dispatcher
        }

    }
}
